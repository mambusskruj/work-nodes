# Helper


## LOCUST

* LOCUST MASTER  
`locust -f test_script.py Test2 --host=http://ais.vidnt.com --no-reset-stats --log-stats --master &`

* LOCUST SLAVES SCRIPT (params: *file*, *num slaves*, *host* (without "http://"))  
`./run_slaves.sh test_script.py Test2 7 localhost`

* RUN LOAD TEST (params: *users*, *ramp-up/sec*, *duration.sec*, *locust host*)  
`./run_load.sh 200 50 600 http://localhost`


## TELEGRAF
`telegraf --config <config_file>`


## TAR 

* ARCHIVATE  
`tar -zcvf archive-name.tar.gz directory-name`

* EXTRACT  
`tar -zxvf archive-name.tar.gz -C /directory-name`


## FIND PHRASE IN FILES

`grep -rnw '/path/to/somewhere/' -e 'pattern'`
* `r` or `-R` is recursive,
* `n` is line number, and
* `w` stands for match the whole word.
* `l` (lower-case L) can be added to just give the file name of matching files.

Along with these, `--exclude`, `--include`, `--exclude-dir` or `--include-dir` flags could be used for efficient searching.


## TAURUS

* RUN TESTCASE  
`bzt tau_test1_turka.yml`


## MONGO

* MONGO DUMP  
`mongodump --host mongodb.example.net --port 27017 --out mongo_back/`

* MONGO RESTORE  
`mongorestore --host mongodb1.example.net --port 27017 mongo_back/`


## GENERATE USERS
`./manage.py --tenant=ais generate_viewers --start=0 --prefix=T 1000000`


